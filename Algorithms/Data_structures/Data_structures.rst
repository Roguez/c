Linked lists
------------

- A linked list is a very flexible, dynamic data structure in which elements (called nodes) form a
  sequential list.

- Linked list acts as a building block to implement data structures such as stacks, queues,
  and their variations.

- A linked list does not store its elements in consecutive memory locations.

- Elements in a linked list can be accessed only in a sequential manner.

- The nodes contains one or more data fields and a pointer to the next node.

- The last node will have no next node connected to it, so it will store a special value called NULL.

- Since in a linked list, every node contains a pointer to another node which is of the same type, it is also
  called a *self-referential* data type.

- Linked lists contain a pointer variable START that stores the address of the first node in the list.

- If START = NULL, then the linked list is empty and contains no nodes.

- Insertions and deletions can be done at any point in the list in a constant time.

- The memory for a node is dynamically allocated when it is added to the list, the total number.
  of nodes that may be added to a list is limited only by the amount of memory available.

- Advantage: Easier to insert or delete data elements.

- Disadvantage: Slow search operation and requires more memory space.

- Write a function that duplicate a linked list, if you need help look the book "C in a nutshell 2E":
  "Pointers as structure members"

- I aslo want that you save the a linked list on a file and recover it.

- I also want that you make a function that print the linked list and then I can test the function 
  for duplicate a linked list and the others to write and read to a file.

- 
  





