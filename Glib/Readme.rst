Libraries
----------
* **GLib**:
    :code:`sudo apt install libglib2.0-dev pkg-config cmake`

* **GTK**: (también hace falta para que esté completa del todo)

    :code:`sudo apt install libgtk2.0-dev pkg-config cmake`

Al poner *pkg-config cmake* estás grabando en *pkg-config*
la dirección de las librerías, así te ahorras tener que
saber donde están y poner la dirección a mano.

Compile
-------

    :code:`gcc -Wall -o filename filename.c $(pkg-config --cflags --libs glib-2.0)`

    *"Wall"* es un  flag para que muestre todos los warnings.

