// https://en.wikipedia.org/wiki/C_data_types
#include<stdio.h>
#include<limits.h>    // for char and integer limits (usr/include/)
#include<float.h>     // for double and float limits (/usr/lib/gcc/x86_64-linux-gnu/7.4.0/include)
#include<stdint.h>    // for intptr_t and uintprt_t
#include<stdbool.h>   // allow bool macro (for using instead of keyword _Bool)
#include<time.h>      // for clock_t
#include<assert.h>    // for assert 
#include"cycle.h"     // for ticks    (non STL)
#include <complex.h>  // allow "complex" and "imaginary" macros (for using 
		      // instead of keywords _Complex, _Imaginary)

# define UCHAR_MIN 0   // limits.h solo define UCHAR_MAX
# define USHRT_MIN 0
# define UINT_MIN 0
# define ULONG_MIN 0
# define ULLONG_MIN 0

// limits.h está en:
// $locate limits.h --> usr/include/limits.h


// A C11 conforming compiler is not obliged to implement complex arithmetic.
// If it does not, it must implement the macro __STDC_NO_COMPLEX__.
//#ifdef __STDC_NO_COMPLEX__
//printf("Complex arithmetic is not supported.\n");
//#else
//printf("Complex arithmetic is supported.\n");
//#endif



int main ()
{

// I want to see that all these declarations works and that I know
// how to use printf with them.

// And I wan to see also that I understand the range and size of 
// every of them.

// size of C primitive data types
        
        long int variable = 99;        
	intptr_t *pointer_NULL = NULL;  
	intptr_t  *pointer = &variable;

	assert(sizeof(pointer_NULL) == sizeof(pointer));

//      Presupongo que la implementación ha dado el mismo tamaño a todas las
//      variables de tipo puntero, independientemente de a que tipo de dato
//      estén apuntando (eso afecta al alineado) entonces en caso de que no
//      quiero que me avise:
//      To print use the same format you'd use for any other values of those types:

	printf("\t [char: %u bits]\n\n", CHAR_BIT);
    printf("\t TYPE                       FORMAT      SIZE(chars)     MIN_VAL    MAX_VAL       PRECISION           Mantisa\n");
	printf("\t                           SPECIFIER                                         (decimal places)\n");
	printf("\t ------------------------------------------------------------------------------------------------------------------------ \n"); 
	printf("\t char                        %%c            %zu           %hhi       %hhi \n", sizeof(char), SCHAR_MIN, SCHAR_MAX);
	printf("\t unsigned char:              %%d            %zu           %hhu       %hhu \n", sizeof(unsigned char), UCHAR_MIN, UCHAR_MAX );
	printf("\t signed char:                              %zu           %hhi       %hhi \n", sizeof(signed char), SCHAR_MIN, SCHAR_MAX);
	printf("\t signed short int:           %%hd           %zu           %hi        %hi  \n", sizeof(signed short int), (short)SHRT_MIN, (short)SHRT_MAX);
	printf("\t unsigned short int          %%hu           %zu           %hu        %hu  \n", sizeof(unsigned short int), (unsigned short)USHRT_MIN, (unsigned short)USHRT_MAX);
        printf("\t signed int: %%d %zu \t %i \t %i \n", sizeof(signed int), INT_MIN, INT_MAX);	
	printf("\t unsigned int: %%u %zu \t %u \t %u \n", sizeof(unsigned int), (unsigned int)UINT_MIN, (unsigned int)UINT_MAX);
	printf("\t signed long int: %%ld %zu \t %li \t %li \n", sizeof(signed long int), (long)LONG_MIN, (long)LONG_MAX);
	printf("\t unsigned long int: %%lu %zu \t %lu \t %lu \n", sizeof(unsigned long int), (unsigned long)ULONG_MIN, (unsigned long)ULONG_MAX);
	printf("\t signed long long int: %%lld %zu \t %lli \t %lli \n", sizeof(signed long long int), (long long)LLONG_MIN, (long long)LLONG_MAX);
	printf("\t unsigned long long int: %%llu %zu \t %llu \t %llu \n", sizeof(unsigned long long int), (unsigned long long)ULLONG_MIN, (unsigned long long)ULLONG_MAX);
	printf("\t ---------------------------------------------------------------------------\n");
	printf("\t float: %%g (%%f, %%e) \t\t %zu \t %g \t %g \t %i \t %i\n", sizeof(float), (float)FLT_MIN, (float)FLT_MAX, FLT_DIG, FLT_MANT_DIG);
	printf("\t double: %%lg (%%lf, %%le)\t\t %zu \t %lg \t %lg \t %i \t %i\n", sizeof(double), (double)DBL_MIN, (double)DBL_MAX, DBL_DIG, DBL_MANT_DIG);
	printf("\t long double: %%Lg (%%Lf, %%Le) \t %zu \t %Lg \t %Lg \t %i \t %i\n", sizeof(long double), (long double)LDBL_MIN, (long double)LDBL_MAX, LDBL_DIG, LDBL_MANT_DIG);
	printf("\t EPSILON represents the difference between 1 and the least value greater than 1 that is representable\n");
	printf("\t FLT_EPSILON: %G DBL_EPSILON: %G LDBL_EPSILON: %LG\n", FLT_EPSILON, DBL_EPSILON, LDBL_EPSILON);
	printf("\t ---------------------------------------------------------------------------\n");
	printf("\t void \t\t %zu\n", sizeof(void*));
	printf("\t size_t \t %zu\n", sizeof(size_t));
	printf("\t intptr_t \t %zu\n", sizeof(intptr_t));   // stdint.h
	printf("\t uintptr_t \t %zu\n", sizeof(uintptr_t));  // stdint.h 
	printf("\t bool \t\t %zu\n", sizeof(bool));
	printf("\t pointer \t %zu\n", sizeof(pointer));
	printf("\t clock_t \t %zu\n", sizeof(clock_t));
	printf("\t ticks \t\t %zu\n", sizeof(ticks));
    
    printf("FLT_MAX %g \t %f \t %i \n", FLT_MIN, FLT_MAX, FLT_DIG);
    printf("DBL_MAX =  %lG\n", DBL_MAX);
    printf("LDBL_MAX = %Lg\n", LDBL_MAX);

   // printf("FLT_MAX = %f\n", FLT_MAX);
   // printf("DBL_MAX = %lf\n", DBL_MAX);
   //  printf("LDBL_MAX = %Lf\n", LDBL_MAX);


    printf("%.*g\n", FLT_DECIMAL_DIG, FLT_MAX);
    printf("%.*g\n", DBL_DECIMAL_DIG, DBL_MAX);
    printf("%.*Lg\n", LDBL_DECIMAL_DIG, LDBL_MAX);
    printf(" The MB_LEN_MAX macro is the maximum number of bytes needed to \n");
    printf(" represent a single wide character, in any of the supported locales.\n");    
    printf(" MB_LEN_MAX: %d \n", MB_LEN_MAX);



    printf("\t Floating points format specifiers: a,e,f,g,x, A, E, F, G, X \n");
    printf("\t a: ..... ");


// -------------------------------------------------------------------------------------------------
//    Jugar con esto:
	    
//  %4f (print as a floating point with a width of at least 4 wide)
//  %.4f (print as a floating point with a precision of four characters after the decimal point)    
//  %3.2f (print as a floating point at least 3 wide and a precision of 2)
// ------------------------------------------------------------------------------------------------

// --------------------------   Declarations   --------------------------------
// char:

   char a = '1'; // Un char puede ser por defecto signed or unsigned, 
                 // también su tamaño de implementación puede variar 
		 // y no ser 8 bits en todas las implementaciones.
		 // Pero sizeof siempre te devolverá 1
		 // Y por defecto para prepresentar los caracteres ascii
		 // se pide como estandar que se declaren como char
		 // Se imprime con %c
		 
   signed char b = -25;    // se imprime con %d
   unsigned char c = 220;  // se imprime con %u y casting a (unsigned)
                           // in printf("%u",ch), ch will be promoted to an int
			   // in normal C implementations. However, the %u specifier
			   // expects an unsigned int, and the C standard does not 
			   // define behavior when the wrong type is passed. It should
			   // instead be: printf("%u", (unsigned) ch);
			   // y date cuenta que debes usar %u porque el valor 220 
			   // te daría overflow si usas un signed. Lo que pasa es que
			   // si en vez de %u usas %d, entonces te funciona bien también
			   // y sin necesidad de hacer el jodido casting (claro porque
			   //  hace el promote a integer!) Bueno yo te pongo las dos 
			   //  formas.  
   //int:

   signed short int d = 1;    //short
   unsigned short int e = 1;  
   signed int f = 1;          //int
   unsigned int g = 1;
   signed long int h = 1;     //long
   unsigned long int i = 1;
   signed long long int j = 1; // long long
   unsigned long long int k = 1;

   //float:
   
   float x;
   double y; 
   long double z; 

   //complex:
   
   float complex fc;
   double complex dc;
   long double complex ldc;
//   float imaginary fi = 2.4*I;
//   double imaginary di = 2.4*I;
//   long double imaginary ldi = 2.4*I;

// Casting an imaginary value to a complex type produces a complex number with a zero real part and a complex
// part the same as the imaginary number. Casting a value of an imaginary type to a real type other than _Bool results in
// 0. Casting a value of an imaginary type to type _Bool results in 0 for a zero imaginary value and 1 otherwise.



// ---------------------------- Strings in C -------------------------------------
   
   char name[50]; // this is a string in C, an array of chars.
                  // then printf or scanf ("%s", name)
		  // sizeof(name) --> 50 nos da el espacio reservado en memoria.
		  // strlen(name) --> Nos da el número de letras del string.
		  //                  Usa la librería string.h

// Sigue por aquí: https://en.wikibooks.org/wiki/C_Programming/String_manipulation

// ----------------------  printing char     --------------------------------------
// %u
// %d
// %c
    printf("CHAR_BIT = %u bits, sizeof = %zu, a = %c\n", CHAR_BIT, sizeof(a), a);
    printf("SCHAR_MIN = %d, sizeof = %zu chars, b = %d\n", SCHAR_MIN, sizeof(b), b);
    printf("SCHAR_MAX = %u, sizeof = %zu chars\n", SCHAR_MAX, sizeof(b));
    printf("UCHAR_MAX = %u, sizeof = %zu chars, c = %u\n", UCHAR_MAX, sizeof(c),(unsigned)c);

// ----------------------  printing int      --------------------------------------

//    1011000000000000000000000000000000000000000000000000
// 10110000000000000000000


// ----------------------  printing float    --------------------------------------
// %f
// %e 
// %E
// %g
// %G
// %a
// %A

    return 0;
}

