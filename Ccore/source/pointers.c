#include<stdio.h>
#include<time.h>
#include "cycle.h"

int main(void) {

	int x, *p;
	x = 24;
	p = &x;
	printf("1)\n");
	printf(" int: %zu, int*: %zu \n", sizeof(x), sizeof(p));		
	printf("x = %d\n", x);
	printf("2)\n");

// -------------------- Volcando los bit de cualquier variable	

        float valor;
	valor = 0.15625;
	float *ptr_float;
	ptr_float = &valor;
        size_t object_size; 
	object_size = sizeof(valor);
	int i = 0;

/*	for(i = 1 ;i <= object_size ;i++ )
	{	
	
        
		
	}
*/

// ------------------------  Trying cycle.h


	ticks start;
	ticks end;
        double delay;
        unsigned char hi;

	start = getticks();
//      hi = getchar();
        printf("Hello World\n");
	end = getticks();

	delay = elapsed(end, start);
        printf("Delay: %lf\n", delay);
	

// --------------09/03/2020
// -------------------------Utilizando %p

       int edad = 25;
       int *ptr_edad = &edad;

       // te imprimen exáctamente lo mismo, y en formato hexadecimal además:

       printf("\t Opcion 1, dirección de edad %p \n", &edad);
       printf("\t Opción 2, dirección de edad %p \n", ptr_edad);
       printf("\t Opción 3, dirección de edad %p \n", (void *)ptr_edad);  // recomendado para evitar problemas de portabilidad.

// 2d array
  int num_2d[3][4] = {
    {1, 2,  3,  4},
    {5, 6,  7,  8},
    {9, 10, 11, 12}
  };
  
  int num_1d[3] = {1,2,3};

  int *ptr_2d = &num_2d[0][0];
  int *ptr_1d = num_1d;

  printf("\t num_2d[2][2]: %i\n ", *(ptr_2d + 5));
  printf("\t num_1d[2]: %i\n ", *ptr_1d + 1);
      
// ----------------------------------------------------------------------------------------------------------------

  int matrix[2][3] = {{1,2,3},{4,5,6}};
  printf("\t matrix[1]: %p\n", matrix[1]);
  printf("\t matrix[1]: %p\n", &matrix[1]);
  int *ptr_row1 = matrix[1];  // ok, no warning
  


return 0;
}

