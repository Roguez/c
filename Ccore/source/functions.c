// Quiero tratar aquí el:
// Pasar una función como argumento, explícalo y luego implementa un caso real con cycle.h
// https://stackoverflow.com/questions/9410/how-do-you-pass-a-function-as-a-parameter-in-c
// Que una función te devuelva varios argumentos, quiero que me lo muestres con estos
// dos ejemplos:
// https://www.techiedelight.com/return-multiple-values-function-c/
// https://www.geeksforgeeks.org/how-to-return-multiple-values-from-a-function-in-c-or-cpp/
// Y que me lo implentes con time.h y así tengo dos comparaciones la de cycle y esta otra y me
// devuelve las dos. Y si hace falta iré devolviendo mas, y ampliando la librería que voy a colgar
// en github para que todo el mundo colabore, al final es guay que tengamos el mismo standard
// para medir el performance de nuestro código, incluso el mismo código en diferentes máquinas.
// compilado de maneras diferentes etc, la verdad que es muy interesante.
