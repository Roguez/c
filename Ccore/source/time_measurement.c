 // We want to measure the time of execution preciselly.
 // That's why I have been thinking about measuring just the
 // clock cycles that the execution need.
 // I think that the best would be just a tool that read the 
 // binary or assembler code and then can add up altogether the 
 // amount of cycles necesaries to execute the code.
 // https://www.geeksforgeeks.org/how-to-measure-time-taken-by-a-program-in-c/
 // But it seems that there is not implemented any solution like this
 // then there are a very popular method, but that:
 // 1) it doesnt give nano precision, son if the time is faster than
 // 1 milisecond then the result would be cero, well that's why then
 // people use the execution of the function that want to measure inside
 // a loop and then divide between the number of executions and get
 // the single result. well that could be ok.
 //
 //** It is important to know that:
 //
 //   There is a problem that "the clock frequency may vary dynamically and 
 //   that measurements are unstable due to interrupts and task switches."
 //   
 //   Entonces, bueno los procesadores intel tienen un registro que es un contador donde se
 //   guardan los ciclos de reloj que se van ejecutando, pero tambien tiene sus problemas, 
 //   sobretodo porque te vale solo para intel, segundo porque para que sea realmente fiable
 //   debes ejecutar el programa en un único procesador, si lo haces multiprocess entonces
 //   pierdes fiabilidad, ya que todos los cores te mostrarán el mismo registro, bueno, 
 //   aquí lo explica:
 //
 //   The Time Stamp Counter was once an excellent high-resolution, low-overhead way for a program 
 //   to get CPU timing information. With the advent of multi-core/hyper-threaded CPUs, systems with
 //   multiple CPUs, and hibernating operating systems, the TSC cannot be relied upon to provide 
 //   accurate results — unless great care is taken to correct the possible flaws: rate of tick and 
 //   whether all cores (processors) have identical values in their time-keeping registers. There is
 //   no promise that the timestamp counters of multiple CPUs on a single motherboard will be 
 //   synchronized. Therefore, a program can get reliable results only by limiting itself to run on 
 //   one specific CPU. Even then, the CPU speed may change because of power-saving measures taken
 //   by the OS or BIOS, or the system may be hibernated and later resumed, resetting the TSC. In
 //   those latter cases, to stay relevant, the program must re-calibrate the counter periodically.
 //   Relying on the TSC also reduces portability, as other processors may not have a similar feature.
 //
 //   Mas info sobre el Time Stamp Counter aquí: https://en.wikipedia.org/wiki/Time_Stamp_Counter
 //
 //   Y finalmente este te lo resume mejor, y te dice que solo un profiler te puede decir cuanto se tardó
//    realmente.
 //
 //   https://stackoverflow.com/questions/22887314/time-taken-to-load-value-from-registers-in-x86
//
//    En cualquier caso estás trabajando con cycles.h que te da dentro de todo una idea comparativa
//      de una implementación frente a otra, y ya con eso puedes estar satisfecho. Es lo que necesitas. 
//      Y además vas a compararlo con el método de time.h y el clock_t y esas cosas que es el mas rápido
//      pero no te da nano segundos y cycle.h si, te da hasta el tiempo de un simple printf("Hello world\n");

