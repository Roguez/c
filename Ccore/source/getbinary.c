#include<stdio.h>

void dump_data(const void *object, size_t size)
{
	 int i;
	 printf("[ \n");
	 for(i = 0; i < size; i++)
 	 {
   	 if (i%4 ==0)
   	 {
     	 printf("@%02X",&((const unsigned char *) object)[i]);
     	 printf("[ ");
   	 }
   	 printf("%02x ", ((const unsigned char *) object)[i] & 0xff);
   	 if ((i+1)%4 == 0)
  	    printf("]\n");
 	 }
 	 printf("]\n");

 	 printf("BINARY FORMAT\n");
 	 for (i = 0; i < size; i++)
 	 {
   	 printf("@%02X",&((const unsigned char *) object)[i]);
   	 printf("[ ");
   	 unsigned char value = (((unsigned char*)object)[i]);
   	 for(int j=0; j<8; j++)
     	 printf("%d ", (value & (0x80 >> j)) ? 1 : 0); // right shifting the value will print bits in reverse.
   	 printf("]\n");
	 }
}

int main(void)
{

	float number;
	number = 0.15252;
	float *ptr_number = &number;
	const void *fucking = ptr_number;
	dump_data(fucking, sizeof(number));
	return 0;

}
