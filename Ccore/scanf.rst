scanf
-----

Reads data from stdin and stores them according to the **parameter format** into the **locations pointed** 
by the additional arguments.

Por eso un:

char name[50]
int age

scanf("%49s", name)  // Le dices que es un string de longitud 49 + \n (supongo) que te lo guarde en "name" ya 
                     // que el nombre de un array es una dirección de memoria.
scanf("%d", &age)    // Le dices que es un entero (el ya sabe la longitud de un entero) y le pasas la dirección
                     // de memoria donde guardarlo que es la de age.

Returning value
---------------



