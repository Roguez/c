Data types to printf
--------------------

.. csv-table:: CSV table
   :header: "Declaring", "Printing", "Lenght", "Description"
   :widths: 15, 15, 15, 15
   
   "size_t", "%zu", "size unsigned", "Used by sizeof(), malloc(), memcpy(), strlen()"
   "
   "
   "
   "
   "
   "
   "






size_t
++++++

// Here argument of 'n' refers to maximum blocks that can be
// allocated which is guaranteed to be non-negative.
void *malloc(size_t n);

// While copying 'n' bytes from 's2' to 's1'
// n must be non-negative integer.
void *memcpy(void *s1, void const *s2, size_t n);

// strlen() uses size_t because the length of any string
// will always be at least 0.
size_t strlen(char const *s);

