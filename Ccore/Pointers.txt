Casting a pointer:
https://prateekvjoshi.com/2014/08/02/what-is-pointer-casting/





- Pointer:
        - Hold address of:
                - Particular data type:
                        - Primitive data types: 
                        - User define data types:
                        - Derived data types:
                        - Void types.
                                - Can't be derefered but can be casted safelly.

- Declaring, derefering and casting a pointer:
        - Declaring a pointer:
                - Data_type* pointer_variable_name
                - And what does it means?
                        - First that pointer_variable_name value is a pointer, which
                          means that it hold a memory address.
                        - And the data_type tells the compiler how many bytes it has
                          to read from that memory address ahead when the pointer is
                          derefered. For example, if it is a char: 1 byte, but if it
                          is and integer: 2 bytes or 4 bytes depending on the 
                          machine. It is usefull to use the built-in function 'sizeof()'
                          to know about it.
        - Dereferencing a pointer:
                - A pointer references a location in memory, and obtaining the value
                  stored at that location is known as dereferencing the pointer. 
                -  The dereference operator (prefix*, not to be confused with the
                  multiplication operator, or with the syntaxis when declaring a pointer)
                  looks up the value that exists at an address.
                        int x, int* p   // declaring
                        p = &x          // & operator "address of"
                        p = 24          // That's wrong
                        *p = 24         // That's ok, this is like x = 24
                        y = *p          // This is deferering now y = 24
		
			- But what happens when:
				- p = 24 // We get a warning:
 					    'assigment makes pointer from integer
					     without a cast'
				- y = p

                         

            



- Printf and pointers










Advantages of void pointers:

1) malloc() and calloc() return void * type and this allows these functions to
be used to allocate memory of any data type (just because of void *)

2) void pointers in C are used to implement generic functions in C.

Propiedades:

1) void pointers cannot be dereferenced. Pero si le hacemos casting a un tipo
   de objeto (int, char etc) entonces si.


---------------------------------------------------------------------------------------------------------
fucking *, has many meannings!!! depending in which kind of statement appear!!!!!!!!!!!!
dont let it confuse you! make a list!!!

1) Multiplicador
   
   c = a * b;

2) Declaración de punteros.
   
   int *d;

3) Indirection operator, refers to the data pointed to by the pointer.
   
   *d = c;
    
    e = *d;

4)  Operador (no sé) sustituye el valor por el del integer correspodiente en los argumentos.

    int margen = 2;
    int precision = 2;
    int grados = 17;

    printf("%*s: %.*f", margen, "Temperatura", precision, grados); 

---------------------------------------------------------------------------------------------------------------------

El puntero tiene dos informaciones:

       1.- El valor de 64 bits que guarda que es un derección de memoria a otra variable.
       2.- El tipo de variable al que apunta que le dice al compilador cual es el tamaño del bloque al que
           apunta el puntero.

Hay dos particularidades:

       1.- Pointer void que puede apuntar a cualquier tipo de variable, pero que debe ser typecast antes 
           de ser utilizado. Son útiles por ejemplo para escribir funciones de propósito general, que 
           acepten diferentes tipos de datos.

       2.- Inicializar un puntero a NULL, que hace que no guarde ninguna posición de memoria válida, es un
           concepto abstracto pero técnicamente es un '0'

POINTERS SHOULD BE ALLWAYS INITIALIZED WHEN ARE DECLARED, IF DOESNT HAVE THE ADDRESS ALREADY USE NULL.


--------------------------------------------------------------------------------------------------------------------

Pointers as function arguments: (pass by pointers)

- function arguments can be of anykind.

- Puedes declarar una función así:
  
  void func(int a, int *bptr) {
          pass
  }     

 y a continuación llamarla así:

  int x = 100, y = 20

  func(x, &y)

 Y ya está, porque lo que el tipo espera con "int *bptr" (es que le pases
 la dirección de un objeto int, y eso lo puedes hacer con un puntero a int
 o &(variable tipo integer)

- "const" también es válido en la declaracion de argumentos, mira que divertido:

   const int *ptr: We can't change de data pointed to by ptr
   int * const ptr: we can't make the pointer point to anywhere else.
   const int * const ptr: Both the data and the pointer to it can't be changed.

Returning Addresses from Functions:

  1- We would have to declare a function returning a pointer

  2- We can't return the value of a local variable becouse it would die at the
     same time that the function finish as it would be auto, so, we have to 
     declare the local variable that we want to return as static.

  So:

  int * myfunction(){      // devuelve la dirección de un objeto tipo int.

        static int r[10];  
        .
	.
        .
	return r;    (le paso la dirección de un objeto int, que es la 
                      posición del primer elemento);
}

  int main(void) {
  
        int *p;
        .
        .
        P = myfunction();


   


Pointers to functions:
   
    Yes, functions are a data type, and we can have pointers to that data type.

---------------------------------------------------------------

Pointers to arrays:

https://www.oreilly.com/library/view/understanding-and-using/9781449344535/ch04.html

 - An array is a contiguous collection of homogeneous elements that can be accessed
   using an index.

 - Array indexes start with 0 and end at one less than their declared size.
   C does not enforce these bounds. Using an invalid index for an array can result 
   in unpredictable behavior.

 - The internal representation of an array has no information about the number of elements 
   it contains. The array name simply references a block of memory. Using the sizeof operator 
   with an array will return the number of bytes allocated to the array. To determine the number
   of elements, we divide the array’s size by its element’s size, as illustrated below. 

  sizeof(array)/sizeof(data_type)

 - Multidimentional array,  in memory is: row1, row2, row2, ....
            
              [R][C]
    int matrix[2][3] = {{1,2,3},{4,5,6}};

 - A two-dimensional array is treated as an array of arrays. That is, when we access the array 
   using only one subscript, we get a pointer to the corresponding row.
 
     Pero como es un array multidimencional no te vale:

              nums_2d[i]    // esto te da:

     Tienes que hacer:

              &num_2d[i]    // esto te da la dirección del primer elemento de la fila i

 - El nombre del array por si mismo es la dirección de memoria al primer elemento. Si eso en el
   unidimensional, con el multidimensional, estoy flipando. Flipa con esto:


     int matrix[2][3] = {{1,2,3},{4,5,6}};

     for (int i = 0; i < 2; i++) {
        printf("&matrix[%d]: %p  sizeof(matrix[%d]): %d\n", 
                i, &matrix[i], i, sizeof(matrix[i]));
     }

    
     el compilador me dice que matrix[i] es un int *,por lo tanto debes usar %p, pero sin 
     embargo porqué usa &matrix[i]!!!!????y sizeof te lo imprime con %d cuando es con %zu
     menuda mierda los cursos de C, con razón es tan difícil aprender jooooder coño!!!

     lo gracioso es que me devuelve la misma dirección con matrix[1] que con &matrix[1], y
     ningún warning, siempre y cuando utilize %p, pero entonces hay que hacer cast a (void*)
     en cualquier caso.

     ¿Pero cómo son lo mismo una cosa que otra?


     es como si el operador & no hiciera nada con un array.
     

      y luego lo de sizeof(matrix[1]), vamos a ver primero es %zu y no %d, 
      y segundo, ...¿que es matrix[1]? un int *, entonces como aplica sizeof
      aplicada a un puntero???? o matrix[1] es un array unidimensional?
      si, es como si todo eso fuera un array unidimensional, entonces puedes hacer:

      int 1Darray[10];
      int 2D_array[10][10];

      pero 2D_array[i] es también un array unidimensional, tienes 10 arrays así. que puedes
      usar exáctamente igual que 1D_array.

      Entonces:

       int *ptr1D = 1Darray;
       int *ptr2D = 2D_array[3]; por ejemplo, esto es que te apunta al cuarto unidimensional array(fila)

       sizeof(1Darray);
       sizeof(2D_array[3]);

       Ok ahora si!

       pero ojo que si fuera un unidemensional array:

       int num[10];

       num[3] no sería un puntero sino un valor (tipo int en este caso), y la dirección sería &num[3] (lógico)











 - Un puntero a un array apunta en consecuencia al primer elemento, y es 
   del mismo tipo que ese elemento del array: int, float, double, el que sea.

 - Un puntero puede apuntar a cualquier miembro de un array:

   int nums[10];
   int *aptr = nums;
   int *iptr = &nums[3];

---------------------------------------------------------------------------------

Pointer arithmetics:

   iptr++    1 es igual al tamaño del objeto al que apunta ese objeto.
   iptr +=4  avanza 4 veces el tamaño del objeto al que apunta ese puntero.
---------------------------------------------------------------------------------

Intercheanbility of Pointers and Arrays:

 -Esto es gracias a pointer arithmetics.

  int nums[ 10 ], *iptr = nums;

   - Esto:

   nums[ 3 ] = 42;

   *( iptr + 3 ) = 42;


   - Es equivalente a esto:

   
   iptr[ 3 ] = 42;

   *( nums + 3 ) = 42;

    - Y esto:

     char array[ ]

    - A esto:

     char *array;

    - Por lo tanto esto:

      void myfunc(const char array[]){}    \\ espera una dirección a un objeto char

    - Es equivalente a esto:
 
      void myfunc(const char *array){}     \\ espera una dirección a un objeto char 


    - Arrays can hold any data type, including pointers. So the declaration:

      int * ptr_1d[ 10 ];

     would create an array of 10 pointers, each of which points to an int.


    - Pointers and multidimensional arrays:

      int num_2d[3][4];

      int *ptr_2d = &num_2d[0][0];

      ¿Lo pillas?

       Un array unidimensional se inicializa así:

       int *ptr_1d = num_1d;

       Pero si es un array multidimensional debes hacerlo así:

       int *ptr_2d = &num_2d[0][0];

       Cualquier otra cosa forma de hacerlo de daría un warning, esto es así porque:

      


-------------------------------------------------------------------------------------------------------

Combinations of * and ++

    *p++ accesses the thing pointed to by p and increments p.
  (*p)++ accesses the thing pointed to by p and increments the thing pointed to by p.
    *++p increments p first, and then accesses the thing pointed to by p.
    ++*p increments the thing pointed to by p first, and then uses it in a larger expression.

Un ejemplo:

This operation takes a value and multiplies it against each element of the vector:


int vector[5] = {1, 2, 3, 4, 5};
int *pv;

pv = vector;
    int value = 3;
    for(int i=0; i<5; i++) {
        *pv++ *= value;
    }

------------------------------------------------------------------------------------------------------------------------

Diferencia entre arrays y punteros:

1)

int vector[5] = {1, 2, 3, 4, 5};

The notation vector[i] generates machine code that starts at location vector, MOVES i positions from this location, and uses its content.

int *pv = vector;

The notation vector+i generates machine code that starts at location vector, ADDS i to the address, and then uses the contents at that address. 

So, we get the same result, but internally is diferent (logical, that internally is diferent)

2) sizeof






-----------------------------------------------------------------------------------------------

Passing one dimensional array to a function.

  - The array’s address is passed by value. 

  - We have to pass also the array size.
          - But in the case of a string stored in an array, we can rely on the NUL termination character
            to tell us when we can stop processing the array.
          - We can declare the array in the function declaration using one of two notations: 
                Array notation:  void displayArray(int arr[], int size) {}   // En realidad es una dirección al objeto elements data_type
                Pointer notation: void displayArray(int* arr, int size) {}   // En realidad es una dirección al objeto elements data_type
          - Independientemente de con que notación lo hayas pasado, dentro lo puedes usar con:
                Array notation: arr[i]
                Pointer notation: *(arr+i)
   
  - Y la llamada sería así de fácil, independientemente de la notación utilizada en la declaración de la función:
                
		int vector[5] = {1, 2, 3, 4, 5};
    	        
                displayArray(vector, 5);

  - Lo del sizeof para pasar el tamaño, porque tienes que pasar el tamaño, recuerda que:

                - para el tamaño de un array unidimensional tienes que: sizeof(vector)/sizeof(int)

-------------------------------------------------------------------------------------------------------

Pointers and multidimensional arrays:

   - Declaración e inicialización:
  
     int matrix[2][5] = {{1,2,3,4,5},{6,7,8,9,10}};

   - Each row of a two-dimensional array can be treated as a one-dimensional array.

   - Te lo enseño:

      for(int i=0; i<2; i++) {
         for(int j=0; j<5; j++) {
            printf("matrix[%d][%d]  Address: %p  Value: %d\n",
                       i, j, &matrix[i][j], matrix[i][j]);
         }
      }

   - We can declare a pointer for use with this array as follows: 

          - The expression, (*ptr_name), declares a pointer to an array.
      
          - int (*pmatrix)[5], declare a pointer to a two-dimensional array of integers with five elements per column.
   
          - we can declare and initialize:

               int (*pmatrix)[5] = matrix;

   - Accesing to the elements:

          -

          -

          -

















-------------------------------------------------------------------------------------------------------

Passing a multidimentional array to a function.

    - we can use one of two notations:

            - void display2DArray(int arr[][5], int rows) {}

            - void display2DArray(int (*arr)[5], int rows) {}

                 - Las dos se invocarían igual:

                     display2DArray(matrix, 2);

                 - Para acceder a los elementos se podría utilizar notación de array:

                     arr[i][j]

                 - ¿y la de puntero? ...



                     



            - void display2DArray(int *arr, int rows, int cols) {}
                    - Y dentro de la función accedemos a los elementos así:
                      
                        *(arr + (i*cols) + j);
             
                    - o así:

                        arr[i][j];

                    - Y para invocar la función:

                        display2DArray(&matrix[0][0], 2, 5)   //flipa, lo de &matrix[0][0] ¿y eso por qué?

                        








