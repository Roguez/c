#include<stdio.h>
#include<math.h>
#include<float.h>

int main(void){

    float radio = 0.0, 
          area  = 0.0,
          max_radio = 0.0;

    int square = 2;

    max_radio = sqrtf((float)(FLT_MAX)/(float)(M_PI));
    printf("\nEl máximo radio es de: %g metros", max_radio);

    printf("\nIntroduzca el radio del cículo en metros: ");
    scanf("%g", &radio);

    if (radio < max_radio)
    {
        area = (float)M_PI * powf(radio,(float)square);
        printf("\nEl área es igual a: %g m2 \n", area);
    }

    else
    {
        printf("\nError, el máximo radio permitido es de: %g m.\n", max_radio);
    }
    
    return 0;
}







